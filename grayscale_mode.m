img = imread('candy.png');

subplot(1,3,1); 
imagesc(img(:,:,1)); 
title('Red'); 

subplot(1,3,2); 
imagesc(img(:,:,2)); 
title('Green'); 

subplot(1,3,3); 
imagesc(img(:,:,3)); 
title('Blue'); 

%Untuk mengubah gambar yang kita masukan menjadi MODE GRAYSCALE, Sobat
%bisa menuliskan perintah "colormap gray" pada Command Windows MATLAB